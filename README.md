# XX-UI 下拉组件库

#### 介绍

XX-UI是基于Vue3.x开发的一套桌面端下拉类组件库，项目提供了NPM安装，方便在你的项目中使用；

```bash
$ npm install kim-xx-ui
```



#### 软件架构

Vue3.x，Vite2.x，MVVM设计模式，SASS，Vue-Router4.x


#### 安装教程

```bash
1.  $ npm install
2.  $ npm run dev
```


#### 个性化定制

支持定制，组件库放在/modules/xx-ui目录下，样式文件在/modules/xx-ui/styles下，按照BEM标准命名的，可以个性化定制，然后通过命令打包：

```bash
$ npm dun build
```

