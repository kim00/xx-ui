import RadioGroup from '../radio/src/radio-group.vue'

RadioGroup.install = (app) => {
  app.component(RadioGroup.name, RadioGroup)
}

export default RadioGroup
