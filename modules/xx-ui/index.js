import XxButton from './button'
import XxButtonGroup from './button-group'
import XxCascader from './cascader'
import XxCascaderPanel from './cascader-panel'
import XxCheckbox from './checkbox'
import XxCheckboxButton from './checkbox-button'
import XxCheckboxGroup from './checkbox-group'
import XxCollapseTransition from './collapse-transition'
import XxDropdown from './dropdown'
import XxDropdownItem from './dropdown-item'
import XxDropdownMenu from './dropdown-menu'
import XxInput from './input'
import XxMenu from './menu'
import XxMenuItem from './menu-item'
import XxMenuItemGroup from './menu-item-group'
import XxPopper from './popper'
import XxRadio from './radio'
import XxRadioButton from './radio-button'
import XxRadioGroup from './radio-group'
import XxScrollbar from './scrollbar'
import XxSubmenu from './submenu'
import XxTag from './tag'
import XxTooltip from './tooltip'
import XxTree from './tree'

import './styles/index.scss'

const COMPONENTS = [
  XxButton,
  XxButtonGroup,
  XxCascader,
  XxCascaderPanel,
  XxCheckbox,
  XxCheckboxButton,
  XxCheckboxGroup,
  XxCollapseTransition,
  XxDropdown,
  XxDropdownItem,
  XxDropdownMenu,
  XxInput,
  XxMenu,
  XxMenuItem,
  XxMenuItemGroup,
  XxPopper,
  XxRadio,
  XxRadioButton,
  XxRadioGroup,
  XxScrollbar,
  XxSubmenu,
  XxTag,
  XxTooltip,
  XxTree,
]

const install = (app, options) => {
  COMPONENTS.forEach(component => {
    app.component(component.name, component)
  })
}

export {
  XxButton,
  XxButtonGroup,
  XxCascader,
  XxCascaderPanel,
  XxCheckbox,
  XxCheckboxButton,
  XxCheckboxGroup,
  XxCollapseTransition,
  XxDropdown,
  XxDropdownItem,
  XxDropdownMenu,
  XxInput,
  XxMenu,
  XxMenuItem,
  XxMenuItemGroup,
  XxPopper,
  XxRadio,
  XxRadioButton,
  XxRadioGroup,
  XxScrollbar,
  XxSubmenu,
  XxTag,
  XxTooltip,
  XxTree,
  install,
}

export default {
  install
}
