import Tooltip from './src/index'

Tooltip.install = (app) => {
  app.component(Tooltip.name, Tooltip)
}

export default Tooltip
