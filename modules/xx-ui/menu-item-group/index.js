import MenuItemGroup from '../menu/src/menuItemGroup.vue'

MenuItemGroup.install = (app) => {
  app.component(MenuItemGroup.name, MenuItemGroup)
}

export default MenuItemGroup
