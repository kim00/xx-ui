import SubMenu from '../menu/src/submenu.vue'

SubMenu.install = (app) => {
  app.component(SubMenu.name, SubMenu)
}

export default SubMenu