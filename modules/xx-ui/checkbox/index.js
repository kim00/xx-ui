import Checkbox from './src/checkbox.vue'

Checkbox.install = (app) => {
  app.component(Checkbox.name, Checkbox)
}

export default Checkbox
