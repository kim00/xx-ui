import { ref, computed, inject, getCurrentInstance, watch, } from 'vue';
import { toTypeString } from '@vue/shared';
import { UPDATE_MODEL_EVENT } from '../../utils/constants';
import { useGlobalConfig } from '../../utils/util';
import { xxFormKey, xxFormItemKey } from '../../form';
export const useCheckboxGroup = () => {
  const ELEMENT = useGlobalConfig();
  const xxForm = inject(xxFormKey, {});
  const xxFormItem = inject(xxFormItemKey, {});
  const checkboxGroup = inject('CheckboxGroup', {});
  const isGroup = computed(() => checkboxGroup && (checkboxGroup === null || checkboxGroup === void 0 ? void 0 : checkboxGroup.name) === 'XxCheckboxGroup');
  const xxFormItemSize = computed(() => {
    return xxFormItem.size;
  });
  return {
    isGroup,
    checkboxGroup,
    xxForm,
    ELEMENT,
    xxFormItemSize,
    xxFormItem,
  };
};
const useModel = (props) => {
  let selfModel = false;
  const { emit } = getCurrentInstance();
  const { isGroup, checkboxGroup } = useCheckboxGroup();
  const isLimitExceeded = ref(false);
  const store = computed(() => { var _a; return checkboxGroup ? (_a = checkboxGroup.modelValue) === null || _a === void 0 ? void 0 : _a.value : props.modelValue; });
  const model = computed({
    get() {
      var _a;
      return isGroup.value
        ? store.value
        : (_a = props.modelValue) !== null && _a !== void 0 ? _a : selfModel;
    },
    set(val) {
      var _a;
      if (isGroup.value && Array.isArray(val)) {
        isLimitExceeded.value = false;
        if (checkboxGroup.min !== undefined && val.length < checkboxGroup.min.value) {
          isLimitExceeded.value = true;
        }
        if (checkboxGroup.max !== undefined && val.length > checkboxGroup.max.value) {
          isLimitExceeded.value = true;
        }
        isLimitExceeded.value === false && ((_a = checkboxGroup === null || checkboxGroup === void 0 ? void 0 : checkboxGroup.changeEvent) === null || _a === void 0 ? void 0 : _a.call(checkboxGroup, val));
      }
      else {
        emit(UPDATE_MODEL_EVENT, val);
        selfModel = val;
      }
    },
  });
  return {
    model,
    isLimitExceeded,
  };
};
const useCheckboxStatus = (props, { model }) => {
  const { isGroup, checkboxGroup, xxFormItemSize, ELEMENT } = useCheckboxGroup();
  const focus = ref(false);
  const size = computed(() => { var _a; return ((_a = checkboxGroup === null || checkboxGroup === void 0 ? void 0 : checkboxGroup.checkboxGroupSize) === null || _a === void 0 ? void 0 : _a.value) || xxFormItemSize.value || ELEMENT.size; });
  const isChecked = computed(() => {
    const value = model.value;
    if (toTypeString(value) === '[object Boolean]') {
      return value;
    }
    else if (Array.isArray(value)) {
      return value.includes(props.label);
    }
    else if (value !== null && value !== undefined) {
      return value === props.trueLabel;
    }
  });
  const checkboxSize = computed(() => {
    var _a;
    const temCheckboxSize = props.size || xxFormItemSize.value || ELEMENT.size;
    return isGroup.value
      ? ((_a = checkboxGroup === null || checkboxGroup === void 0 ? void 0 : checkboxGroup.checkboxGroupSize) === null || _a === void 0 ? void 0 : _a.value) || temCheckboxSize
      : temCheckboxSize;
  });
  return {
    isChecked,
    focus,
    size,
    checkboxSize,
  };
};
const useDisabled = (props, { model, isChecked }) => {
  const { xxForm, isGroup, checkboxGroup } = useCheckboxGroup();
  const isLimitDisabled = computed(() => {
    var _a, _b;
    const max = (_a = checkboxGroup.max) === null || _a === void 0 ? void 0 : _a.value;
    const min = (_b = checkboxGroup.min) === null || _b === void 0 ? void 0 : _b.value;
    return !!(max || min) && (model.value.length >= max && !isChecked.value) ||
      (model.value.length <= min && isChecked.value);
  });
  const isDisabled = computed(() => {
    var _a;
    const disabled = props.disabled || xxForm.disabled;
    return isGroup.value
      ? ((_a = checkboxGroup.disabled) === null || _a === void 0 ? void 0 : _a.value) || disabled || isLimitDisabled.value
      : props.disabled || xxForm.disabled;
  });
  return {
    isDisabled,
    isLimitDisabled,
  };
};
const setStoreValue = (props, { model }) => {
  function addToStore() {
    if (Array.isArray(model.value) &&
      !model.value.includes(props.label)) {
      model.value.push(props.label);
    }
    else {
      model.value = props.trueLabel || true;
    }
  }
  props.checked && addToStore();
};
const useEvent = (props, { isLimitExceeded }) => {
  const { xxFormItem } = useCheckboxGroup();
  const { emit } = getCurrentInstance();
  function handleChange(e) {
    var _a, _b;
    if (isLimitExceeded.value)
      return;
    const target = e.target;
    const value = target.checked
      ? (_a = props.trueLabel) !== null && _a !== void 0 ? _a : true
      : (_b = props.falseLabel) !== null && _b !== void 0 ? _b : false;
    emit('change', value, e);
  }
  watch(() => props.modelValue, val => {
    var _a;
    (_a = xxFormItem.formItemMitt) === null || _a === void 0 ? void 0 : _a.emit('xx.form.change', [val]);
  });
  return {
    handleChange,
  };
};
export const useCheckbox = (props) => {
  const { model, isLimitExceeded } = useModel(props);
  const { focus, size, isChecked, checkboxSize } = useCheckboxStatus(props, { model });
  const { isDisabled } = useDisabled(props, { model, isChecked });
  const { handleChange } = useEvent(props, { isLimitExceeded });
  setStoreValue(props, { model });
  return {
    isChecked,
    isDisabled,
    checkboxSize,
    model,
    handleChange,
    focus,
    size,
  };
};
