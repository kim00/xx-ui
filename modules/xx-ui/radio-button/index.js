import RadioButton from '../radio/src/radio-button.vue'

RadioButton.install = (app) => {
  app.component(RadioButton.name, RadioButton)
}

export default RadioButton
