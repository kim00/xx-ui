import CheckboxButton from '../checkbox/src/checkbox-button.vue'

CheckboxButton.install = (app) => {
  app.component(CheckboxButton.name, CheckboxButton)
}

export default CheckboxButton
