import { h, inject, ref, watch, onMounted, onUpdated, onBeforeUnmount, nextTick, Fragment, } from 'vue';
import { xxFormKey, xxFormItemKey, } from './token';
import { addResizeListener, removeResizeListener, } from '../../utils/resize-event';
export default {
  name: 'XxLabelWrap',
  props: {
    isAutoWidth: Boolean,
    updateAll: Boolean,
  },
  setup(props, { slots }) {
    const xx = ref(null);
    const xxForm = inject(xxFormKey);
    const xxFormItem = inject(xxFormItemKey);
    const computedWidth = ref(0);
    watch(computedWidth, (val, oldVal) => {
      if (props.updateAll) {
        xxForm.registerLabelWidth(val, oldVal);
        xxFormItem.updateComputedLabelWidth(val);
      }
    });
    const getLabelWidth = () => {
      var _a;
      if ((_a = xx.value) === null || _a === void 0 ? void 0 : _a.firstElementChild) {
        const width = window.getComputedStyle(xx.value.firstElementChild)
          .width;
        return Math.ceil(parseFloat(width));
      }
      else {
        return 0;
      }
    };
    const updateLabelWidth = (action = 'update') => {
      nextTick(() => {
        if (slots.default && props.isAutoWidth) {
          if (action === 'update') {
            computedWidth.value = getLabelWidth();
          }
          else if (action === 'remove') {
            xxForm.deregisterLabelWidth(computedWidth.value);
          }
        }
      });
    };
    const updateLabelWidthFn = () => updateLabelWidth('update');
    onMounted(() => {
      addResizeListener(xx.value.firstElementChild, updateLabelWidthFn);
      updateLabelWidthFn();
    });
    onUpdated(updateLabelWidthFn);
    onBeforeUnmount(() => {
      updateLabelWidth('remove');
      removeResizeListener(xx.value.firstElementChild, updateLabelWidthFn);
    });
    function render() {
      var _a, _b;
      if (!slots)
        return null;
      if (props.isAutoWidth) {
        const autoLabelWidth = xxForm.autoLabelWidth;
        const style = {};
        if (autoLabelWidth && autoLabelWidth !== 'auto') {
          const marginLeft = parseInt(autoLabelWidth, 10) - computedWidth.value;
          if (marginLeft) {
            style.marginLeft = marginLeft + 'px';
          }
        }
        return h('div', {
          ref: xx,
          class: ['xx-form-item__label-wrap'],
          style,
        }, (_a = slots.default) === null || _a === void 0 ? void 0 : _a.call(slots));
      }
      else {
        return h(Fragment, { ref: xx }, (_b = slots.default) === null || _b === void 0 ? void 0 : _b.call(slots));
      }
    }
    return render;
  },
};
