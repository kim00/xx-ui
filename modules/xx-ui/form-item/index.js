import FormItem from '../form/src/form-item.vue'

FormItem.install = (app) => {
  app.component(FormItem.name, FormItem)
}

export default FormItem
