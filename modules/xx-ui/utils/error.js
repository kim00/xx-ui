class XXError extends Error {
  constructor(m) {
      super(m);
      this.name = 'XXError';
  }
}
export default (scope, m) => {
  throw new XXError(`[${scope}] ${m}`);
};
export function warn(scope, m) {
  console.warn(new XXError(`[${scope}] ${m}`));
}
