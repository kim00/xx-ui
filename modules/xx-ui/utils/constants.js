export const UPDATE_MODEL_EVENT = 'update:modelValue';
export const CHANGE_EVENT = 'change';
export const INPUT_EVENT = 'input';
export const VALIDATE_STATE_MAP = {
  validating: 'xx-icon-loading',
  success: 'xx-icon-circle-check',
  error: 'xx-icon-circle-close',
};
