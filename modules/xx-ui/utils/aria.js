export const EVENT_CODE = {
  tab: 'Tab',
  enter: 'Enter',
  space: 'Space',
  left: 'ArrowLeft',
  up: 'ArrowUp',
  right: 'ArrowRight',
  down: 'ArrowDown',
  esc: 'Escape',
  delete: 'Delete',
  backspace: 'Backspace',
};
const FOCUSABLE_ELEMENT_SELECTORS = `a[href],button:not([disabled]),button:not([hidden]),:not([tabindex="-1"]),input:not([disabled]),input:not([type="hidden"]),select:not([disabled]),textarea:not([disabled])`;

export const isVisible = (el) => {
  if (process.env.NODE_ENV === 'test')
    return true;
  const computed = getComputedStyle(el);

  return computed.position === 'fixed' ? false : el.offsetParent !== null;
};
export const obtainAllFocusableElements = (el) => {
  return Array.from(el.querySelectorAll(FOCUSABLE_ELEMENT_SELECTORS)).filter(isFocusable)
    .filter(isVisible);
};

export const isFocusable = (el) => {
  if (el.tabIndex > 0 ||
    (el.tabIndex === 0 && el.getAttribute('tabIndex') !== null)) {
    return true;
  }

  if (el.disabled) {
    return false;
  }
  switch (el.nodeName) {
    case 'A': {

      return !!el.href && el.rel !== 'ignore';
    }
    case 'INPUT': {
      return !(el.type === 'hidden' || el.type === 'file');
    }
    case 'BUTTON':
    case 'SELECT':
    case 'TEXTAREA': {
      return true;
    }
    default: {
      return false;
    }
  }
};

export const attemptFocus = (el) => {
  var _a;
  if (!isFocusable(el)) {
    return false;
  }
  Utils.IgnoreUtilFocusChanges = true;
  // Remove the old try catch block since there will be no error to be thrown
  (_a = el.focus) === null || _a === void 0 ? void 0 : _a.call(el);
  Utils.IgnoreUtilFocusChanges = false;
  return document.activeElement === el;
};

export const triggerEvent = function (elm, name, ...opts) {
  let eventName;
  if (name.includes('mouse') || name.includes('click')) {
    eventName = 'MouseEvents';
  }
  else if (name.includes('key')) {
    eventName = 'KeyboardEvent';
  }
  else {
    eventName = 'HTMLEvents';
  }
  const evt = document.createEvent(eventName);
  evt.initEvent(name, ...opts);
  elm.dispatchEvent(evt);
  return elm;
};
const Utils = {
  IgnoreUtilFocusChanges: false,

  focusFirstDescendant: function (el) {
    for (let i = 0; i < el.childNodes.length; i++) {
      const child = el.childNodes[i];
      if (attemptFocus(child) ||
        this.focusFirstDescendant(child)) {
        return true;
      }
    }
    return false;
  },

  focusLastDescendant: function (el) {
    for (let i = el.childNodes.length - 1; i >= 0; i--) {
      const child = el.childNodes[i];
      if (attemptFocus(child) ||
        this.focusLastDescendant(child)) {
        return true;
      }
    }
    return false;
  },
};
export default Utils;
