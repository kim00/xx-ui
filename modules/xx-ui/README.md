# XX-UI 下拉组件库

#### 介绍

XX-UI是基于Vue3.x开发的一套桌面端下拉类组件库，项目已经在[GItee](https://gitee.com/kim00/xx-ui) 上开源，可以拉取项目做个性化定制。

#### 项目包含以下八个组件

1. Menu 导航菜单

2. Dropdown 下拉菜单

3. Cascader 级联选择

4. Tree 树形控件

5. Input 输入框

6. Radio 单选框

7. Checkbox 多选框

8. Button 按钮

#### 软件架构

Vue3.x，Vite2.x，MVVM设计模式，SASS，Vue-Router4.x

注：XX-UI开发文档，目前还未部署到服务器，可以去[GItee](https://gitee.com/kim00/xx-ui) 上拉取完整项目并运行即可看到开发文档。

