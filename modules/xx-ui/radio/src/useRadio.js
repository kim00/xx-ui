import { ref, computed, inject } from 'vue';
import { xxFormKey, xxFormItemKey } from '../../form';
import { useGlobalConfig } from '../../utils/util';
export const useRadio = () => {
  const ELEMENT = useGlobalConfig();
  const xxForm = inject(xxFormKey, {});
  const xxFormItem = inject(xxFormItemKey, {});
  const radioGroup = inject('RadioGroup', {});
  const focus = ref(false);
  const isGroup = computed(() => (radioGroup === null || radioGroup === void 0 ? void 0 : radioGroup.name) === 'XxRadioGroup');
  const xxFormItemSize = computed(() => xxFormItem.size || ELEMENT.size);
  return {
    isGroup,
    focus,
    radioGroup,
    xxForm,
    ELEMENT,
    xxFormItemSize,
  };
};
export const useRadioAttrs = (props, { isGroup, radioGroup, xxForm, model, }) => {
  const isDisabled = computed(() => {
    return isGroup.value
      ? radioGroup.disabled || props.disabled || xxForm.disabled
      : props.disabled || xxForm.disabled;
  });
  const tabIndex = computed(() => {
    return (isDisabled.value || (isGroup.value && model.value !== props.label)) ? -1 : 0;
  });
  return {
    isDisabled,
    tabIndex,
  };
};
