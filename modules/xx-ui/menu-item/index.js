import MenuItem from '../menu/src/menuItem.vue'

MenuItem.install = (app) => {
  app.component(MenuItem.name, MenuItem)
}

export default MenuItem