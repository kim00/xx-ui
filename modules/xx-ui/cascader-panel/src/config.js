import { computed } from 'vue';
import { NOOP } from '@vue/shared';
export const CommonProps = {
  modelValue: [Number, String, Array],
  options: {
    type: Array,
    default: () => [],
  },
  props: {
    type: Object,
    default: () => ({}),
  },
};
export const DefaultProps = {
  expandTrigger: 'click',
  multiple: false,
  checkStrictly: false,
  emitPath: true,
  lazy: false,
  lazyLoad: NOOP,
  value: 'value',
  label: 'label',
  children: 'children',
  leaf: 'leaf',
  disabled: 'disabled',
  hoverThreshold: 500,
};
export const useCascaderConfig = (props) => {
  return computed(() => (Object.assign(Object.assign({}, DefaultProps), props.props)));
};
