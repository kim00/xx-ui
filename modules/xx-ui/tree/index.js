import Tree from './src/tree.vue'

Tree.install = (app) => {
  app.component(Tree.name, Tree)
}

export default Tree
