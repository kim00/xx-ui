import Scrollbar from './src/index.vue'

Scrollbar.install = (app) => {
  app.component(Scrollbar.name, Scrollbar)
}

export default Scrollbar
