import CollapseTransition from '../transition/collapse-transition/index.vue'

CollapseTransition.install = (app) => {
  app.component(CollapseTransition.name, CollapseTransition)
}

export default CollapseTransition
