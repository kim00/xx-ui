export default function buildModifier(props, externalModifiers = []) {
    const { arrow, arrowOffset, offset, gpuAcceleration, } = props;
    const modifiers = [
        {
            name: 'offset',
            options: {
                offset: [0, offset !== null && offset !== void 0 ? offset : 12],
            },
        },
        {
            name: 'preventOverflow',
            options: {
                padding: {
                    top: 2,
                    bottom: 2,
                    left: 5,
                    right: 5,
                },
            },
        },
        {
            name: 'flip',
            options: {
                padding: 5,
            },
        },
        {
            name: 'computeStyles',
            options: {
                gpuAcceleration,
                adaptive: gpuAcceleration,
            },
        },
        // tippyModifier,
    ];
    if (arrow) {
        modifiers.push({
            name: 'arrow',
            options: {
                element: arrow,
                padding: arrowOffset !== null && arrowOffset !== void 0 ? arrowOffset : 5,
            },
        });
    }
    modifiers.push(...(externalModifiers));
    return modifiers;
}
