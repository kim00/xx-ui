import { computed } from 'vue';
import buildModifiers from './build-modifiers';
export default function usePopperOptions(props, state) {
    return computed(() => {
        var _a;
        return Object.assign(Object.assign({ placement: props.placement }, props.popperOptions), {
            // Avoiding overriding modifiers.
            modifiers: buildModifiers({
                arrow: state.arrow.value,
                arrowOffset: props.arrowOffset,
                offset: props.offset,
                gpuAcceleration: props.gpuAcceleration,
            }, (_a = props.popperOptions) === null || _a === void 0 ? void 0 : _a.modifiers)
        });
    });
}
