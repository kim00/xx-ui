import { cloneVNode } from 'vue';
import throwError from '../../../utils/error';
import { getFirstValidNode } from '../../../utils/vnode';
export default function renderTrigger(trigger, extraProps) {
  const firstElement = getFirstValidNode(trigger, 1);
  if (!firstElement)
    throwError('renderTrigger', 'trigger expects single rooted node');
  return cloneVNode(firstElement, extraProps, true);
}
