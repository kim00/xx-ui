import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import Home from '../views/index.vue'
import Button from '../views/Button/index.vue'
import Cascader from '../views/Cascader/index.vue'
import Checkbox from '../views/Checkbox/index.vue'
import Dropdown from '../views/Dropdown/index.vue'
import Form from '../views/Form/index.vue'
import Guide from '../views/Guide/index.vue'
import Input from '../views/Input/index.vue'
import Introduction from '../views/Introduction/index.vue'
import Menu from '../views/Menu/index.vue'
import Radio from '../views/Radio/index.vue'
import Tree from '../views/Tree/index.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path: 'introduction',
        component: Introduction,
      }, {
        path: 'button',
        component: Button,
      }, {
        path: 'cascader',
        component: Cascader,
      }, {
        path: 'checkbox',
        component: Checkbox,
      }, {
        path: 'dropdown',
        component: Dropdown,
      }, {
        path: 'form',
        component: Form,
      }, {
        path: 'guide',
        component: Guide,
      }, {
        path: 'input',
        component: Input,
      }, {
        path: 'menu',
        component: Menu,
      }, {
        path: 'radio',
        component: Radio,
      }, {
        path: 'tree',
        component: Tree,
      }
    ],
    redirect: { path: '/introduction' }
  }
]

const scrollBehavior = function (to, from, savedPosition) {
  if (to.hash) {
    return {
      // 通过 to.hash 的值來找到对应的元素
      el: to.hash,
      behavior: 'smooth',
      top: 90,
      left: 0,
    }
  }
}

const router = createRouter({
  // history: createWebHistory(process.env.BASE_URL),
  history: createWebHistory(),
  // history: createWebHashHistory(),
  routes,
  scrollBehavior
})

export default router
