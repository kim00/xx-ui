import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import XxUI from '@xx'
import hljs from 'highlight.js'
import 'highlight.js/scss/atom-one-light.scss'

// import { TreeMenu, MenuItem } from '@xx'
// const XxUIConfig = {
//   components: [
//     'XxInput',
//   ]
// }

const app = createApp(App)

app.directive('hljs', (el) => { hljs.highlightBlock(el); })
app.use(XxUI)
app.use(router)
app.mount('#app')
